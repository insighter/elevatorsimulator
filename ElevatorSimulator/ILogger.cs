﻿using System;

namespace ElevatorSimulator
{
    public interface ILogger
    {
        void LogError(string message);
        void LogException(Exception ex);
    }
}
