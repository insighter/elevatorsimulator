﻿using System;
using System.Collections.Concurrent;
using System.Threading;

namespace ElevatorSimulator
{
    class Program
    {
        static void Main(string[] args)
        {
            var parameterConstraints = new ParameterConstraints();            

            if (args.Length < 4)
            {
                Console.WriteLine($"Usage: ElevatorSimulator.exe <floorCount({parameterConstraints.FloorCountMin}..{parameterConstraints.FloorCountMax})> " +
                    $"<floorHeightMetersDecimal({parameterConstraints.FloorHeightMin}..{parameterConstraints.FloorHeightMax})> " +
                    $"<elevatorSpeedMetersPerSecond({parameterConstraints.ElevatorSpeedMin}..{parameterConstraints.ElevatorSpeedMax})> "+
                    $"<openedDoorWaitTimeSeconds({parameterConstraints.WaitTimeMin}..{parameterConstraints.WaitTimeMax})>");
                return;
            }

            ILogger logger = new Logger();
            InputParameters parameters;
            if (!TryParseCommandline(args, parameterConstraints, logger, out parameters))
            {
                logger.LogError("Exit because commandline parameters are wrong");
                return;
            }

            var userActions = new ConcurrentQueue<int>();
            var userActionEvent = new AutoResetEvent(false);
            var cancelTokenSource = new CancellationTokenSource();

            var elevatorTask = Elevator.RunNewElevator(parameters, userActions, userActionEvent, cancelTokenSource.Token);

            var inputProcessor = new UserInputProcessor(logger);
            inputProcessor.ProcessUserInput(parameters, userActions, userActionEvent, cancelTokenSource, elevatorTask);            
        }
      
        private static bool TryParseCommandline(string[] args, ParameterConstraints parameterConstraints, ILogger logger, 
            out InputParameters parameters)
        {
            parameters = new InputParameters();

            int floorCount;
            if (!int.TryParse(args[0], out floorCount))
            {
                logger.LogError("Invalid floor count");
                return false;
            }

            if (floorCount < parameterConstraints.FloorCountMin || floorCount > parameterConstraints.FloorCountMax)
            {
                logger.LogError($"Floor count is out of range ({parameterConstraints.FloorCountMin}..{parameterConstraints.FloorCountMax})");
                return false;
            }

            parameters.FloorCount = floorCount;

            float floorHeight;
            if (!float.TryParse(args[1], out floorHeight))
            {
                logger.LogError("Invalid floor height");
                return false;
            }
            if (floorHeight < parameterConstraints.FloorHeightMin || floorHeight > parameterConstraints.FloorHeightMax)
            {
                logger.LogError($"Floor height is out of range ({parameterConstraints.FloorHeightMin}..{parameterConstraints.FloorHeightMax})");
                return false;
            }

            parameters.FloorHeight = floorHeight;

            float elevatorSpeed;
            if (!float.TryParse(args[2], out elevatorSpeed))
            {
                logger.LogError("Invalid elevator speed");
                return false;
            }
            if (elevatorSpeed < parameterConstraints.ElevatorSpeedMin || elevatorSpeed > parameterConstraints.ElevatorSpeedMax)
            {
                logger.LogError($"Elevator speed is out of range ({parameterConstraints.ElevatorSpeedMin}..{parameterConstraints.ElevatorSpeedMax})");
                return false;
            }

            parameters.ElevatorSpeed = elevatorSpeed;

            int waitTime;
            if (!int.TryParse(args[3], out waitTime))
            {
                logger.LogError("Invalid opened door wait time");
                return false;
            }
            if (waitTime < parameterConstraints.WaitTimeMin || waitTime > parameterConstraints.WaitTimeMax)
            {
                logger.LogError($"Opened door wait time is out of range ({parameterConstraints.WaitTimeMin}..{parameterConstraints.WaitTimeMax})");
                return false;
            }

            parameters.WaitTime = waitTime;

            return true;
        }           
    }
}
