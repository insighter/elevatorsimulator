﻿namespace ElevatorSimulator
{
    public struct InputParameters
    {    
        public int FloorCount { get; set; }
        public float FloorHeight { get; set; }
        public float ElevatorSpeed { get; set; }
        public int WaitTime { get; set; }
    }
}
