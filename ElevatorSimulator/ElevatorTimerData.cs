﻿using System.Collections.Concurrent;
using System.Threading;

namespace ElevatorSimulator
{
    public class ElevatorTimerData
    {
        public ConcurrentQueue<int> UserActions { get; set; }
        public int TargetFloor { get; set; }
        public int DoorWaitTime { get; set; }
        public EventWaitHandle FloorArrivedEvent { get; set; }
        public CancellationToken CancellationToken { get; set; }
    }
}
