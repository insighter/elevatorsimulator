﻿namespace ElevatorSimulator
{
    public struct ParameterConstraints
    {
        public int FloorCountMax { get { return 20; } }
        public int FloorCountMin { get { return 5; } }
        public float FloorHeightMax { get { return 10; } }
        public float FloorHeightMin { get { return 2.5F; } }
        public float ElevatorSpeedMax { get { return 17F; } }
        public float ElevatorSpeedMin { get { return 0.18F; } }
        public int WaitTimeMax { get { return 30; } }
        public int WaitTimeMin { get { return 5; } }
    }
}
