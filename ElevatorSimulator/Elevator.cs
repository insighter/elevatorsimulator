﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace ElevatorSimulator
{
    public class Elevator
    {       
        private int _currentFloor = 1;
        private Timer _processMovingTimer;

        public static Task RunNewElevator(InputParameters parameters, ConcurrentQueue<int> userActions, AutoResetEvent userActionEvent, 
            CancellationToken cancellationToken)
        {
            var elevator = new Elevator();

            return Task.Factory.StartNew(() => elevator.OperateElevator(
                parameters.FloorCount, 
                parameters.FloorHeight, 
                parameters.ElevatorSpeed, 
                parameters.WaitTime,
                userActions,
                userActionEvent,
                cancellationToken),
                cancellationToken);
        }

        private void OperateElevator(int floorCount, float height, float speed, int waitTime, 
            ConcurrentQueue<int> userActions, EventWaitHandle userActionEvent, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();                       

            var timeBetweenFloorsSec = height / speed;
            int timeBetweenFloorsMilliseconds = (int)(timeBetweenFloorsSec * 1000F);
            var processMoving = new TimerCallback(ProcessMoving);

            while (true)
            {
                WaitIfNoUserActions(userActions, userActionEvent);

                if (cancellationToken.IsCancellationRequested)
                {
                    break;
                }

                int toFloor;
                if (!userActions.TryPeek(out toFloor))
                {
                    continue;
                }   

                var floorArrivedEvent = new AutoResetEvent(false);
                var timerData = new ElevatorTimerData
                {
                    UserActions = userActions,
                    TargetFloor = toFloor,
                    DoorWaitTime = waitTime,
                    FloorArrivedEvent = floorArrivedEvent,
                    CancellationToken = cancellationToken
                };
                _processMovingTimer = new Timer(processMoving, timerData, 0, timeBetweenFloorsMilliseconds);
                floorArrivedEvent.WaitOne();
            }
        }

        private void ProcessMoving(object data)
        {
            var timerData = (ElevatorTimerData)data;
            var userActions = timerData.UserActions;
            var targetFloor = Math.Abs(timerData.TargetFloor);
            var waitTime = timerData.DoorWaitTime;
            var floorArrivedEvent = timerData.FloorArrivedEvent;
            var cancellationToken = timerData.CancellationToken;

            if (cancellationToken.IsCancellationRequested)
            {
                StopElevator();
                floorArrivedEvent.Set();
                return;
            }

            if (IsTargetFloorCurrent(targetFloor))
            {
                StopElevator();
                OpenDoors();
                WaitOpenDoorsInterval(waitTime);
                CloseDoors();
                RemoveProcessedUserAction(userActions);
                floorArrivedEvent.Set();
            }
            else
            {
                Output($"Elevator passed the floor: {_currentFloor}");

                if (IsDestinationUp(targetFloor))
                {
                    GoToNextUpFloor();
                }
                else
                {
                    GoToNextDownFloor();
                }
            }
        }

        private bool IsDestinationUp(int targetFloor)
        {
            return targetFloor > _currentFloor;
        }

        private void GoToNextDownFloor()
        {
            Interlocked.Decrement(ref _currentFloor);
        }

        private void GoToNextUpFloor()
        {
            Interlocked.Increment(ref _currentFloor);            
        }

        private void WaitIfNoUserActions(ConcurrentQueue<int> userActions, EventWaitHandle userActionEvent)
        {
            if (userActions.IsEmpty)
            {
                userActionEvent.WaitOne();
            }
        }

        private bool IsTargetFloorCurrent(int targetFloor)
        {
            return targetFloor == _currentFloor;
        }

        private void RemoveProcessedUserAction(ConcurrentQueue<int> userActions)
        {
            int dummy;
            userActions.TryDequeue(out dummy);
        }

        private void StopElevator()
        {
            _processMovingTimer.Change(Timeout.Infinite, Timeout.Infinite);            
            Output($"Elevator stopped at the floor: {_currentFloor}");
        }

        private void WaitOpenDoorsInterval(int waitTime)
        {
            var waitEvent = new ManualResetEvent(false);
            waitEvent.WaitOne(waitTime * 1000);
        }

        private void OpenDoors()
        {            
            Output($"Elevator opened doors at the floor: {_currentFloor}");
        }

        private void CloseDoors()
        {         
            Output($"Elevator closed doors at the floor: {_currentFloor}");
        }

        private void Output(string message)
        {
            Console.WriteLine(message);
        }
    }
}
