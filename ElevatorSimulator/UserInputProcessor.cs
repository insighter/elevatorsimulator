﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Threading;

namespace ElevatorSimulator
{
    public class UserInputProcessor
    {
        private ILogger _logger;

        public UserInputProcessor(ILogger logger)
        {
            _logger = logger;
        }

        public void ProcessUserInput(InputParameters parameters, ConcurrentQueue<int> userActions, AutoResetEvent userActionEvent, CancellationTokenSource cancelSrc, Task elevatorTask)
        {
            Console.WriteLine("Input floor number and press Enter:");
            Console.WriteLine("* positive value: user calls elevator to the floor (e.g. input 5)");
            Console.WriteLine("* negative value: user presses floor button inside the elevator (e.g. input -5)");
            Console.WriteLine("* zero means stop the application");

            while (true)
            {
                var userInput = Console.ReadLine();
                int floor;
                var isInputOk = int.TryParse(userInput, out floor);
                if (isInputOk)
                {
                    var absFloor = Math.Abs(floor);
                    if (absFloor > 0 && absFloor <= parameters.FloorCount)
                    {
                        userActions.Enqueue(floor);
                        userActionEvent.Set();
                    }
                    else
                    {
                        if (absFloor == 0)
                        {
                            StopElevator(userActionEvent, cancelSrc, elevatorTask);
                            break;
                        }
                        isInputOk = false;
                    }
                }

                if (!isInputOk)
                {
                    _logger.LogError("Invalid user action");
                }
            }
        }

        private void StopElevator(AutoResetEvent userActionEvent, CancellationTokenSource cancelSrc, Task elevatorTask)
        {
            try
            {
                cancelSrc.Cancel();
                userActionEvent.Set();
                elevatorTask.Wait();
            }
            catch (AggregateException ex)
            {
                _logger.LogException(ex);
            }
            finally
            {
                elevatorTask.Dispose();
                cancelSrc.Dispose();
            }
        }
    }
}
