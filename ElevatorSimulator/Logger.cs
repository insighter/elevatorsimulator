﻿using System;

namespace ElevatorSimulator
{
    public class Logger : ILogger
    {
        public void LogError(string message)
        {
            Console.Error.WriteLine(message);
        }

        public void LogException(Exception ex)
        {
            LogError(ex.Message);
        }
    }
}
